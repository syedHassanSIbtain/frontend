import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginFormControl : FormGroup;
  constructor(private _router : Router) {
    this.loginFormControl = new FormGroup({
      Username: new FormControl('test123', [Validators.required]),
      Password: new FormControl('test', [Validators.required]),
      rememberMe: new FormControl(false),
      grant_type: new FormControl('password'),
    });
   }

  ngOnInit(): void {
  }
  Login(){
    this._router.navigate(['/user/userList'])
  }
}
