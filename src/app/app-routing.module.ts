import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path : 'auth',
    loadChildren : () => import('./auth/auth.module').then(val=>val.AuthModule)
  },
  {
    path : 'user',
    loadChildren : () => import('./user/user.module').then(val=>val.UserModule)
  },
  {
    path:'**',
    redirectTo : 'auth'
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
