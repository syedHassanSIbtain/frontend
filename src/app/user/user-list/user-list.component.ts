import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsersService } from '../users.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  studentList: any = []
  constructor(private _UserService: UsersService, private _router: Router) {
    this.getAll()
  }

  ngOnInit(): void {
  }
  open(id) {
    this._router.navigate(['/user/update/' + id])
  }
  getAll() {
    this._UserService.getCalls('getAll')
      .then((val: any) => {
        this.studentList = val;
      })
  }
  delete(id) {
    this._UserService.getCalls('delete/' + id)
      .then(val => {
        if (val) {
          this.getAll()
        }
      })
  }
}
