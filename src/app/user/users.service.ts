import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  urlTest = "http://localhost:5000/"
  constructor(private _http : HttpClient) {

  }

  getCalls(endPoint : string){
    return new Promise((resolve,reject)=>{
      const url = this.urlTest + endPoint;
      this._http.get(url)
      .toPromise()
      .then(val=>{
        resolve(val)
      })
      .catch(error=>{
        reject(error)
      })
    })
  }
  postCalls(endPoint : string,Obj : any){
    return new Promise((resolve,reject)=>{
      const url = this.urlTest + endPoint;
      this._http.post(url,Obj)
      .toPromise()
      .then(val=>{
        resolve(val)
      })
      .catch(error=>{
        reject(error)
      })
    })
  }

}
