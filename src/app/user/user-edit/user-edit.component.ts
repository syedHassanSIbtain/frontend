import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UsersService } from '../users.service';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss']
})
export class UserEditComponent implements OnInit {

  studentCreateForm: FormGroup;
  departmentList: any = [
    { ID: 0, Name: 'Science' },
    { ID: 1, Name: 'Arts' },
    { ID: 2, Name: 'Maths' },
    { ID: 3, Name: 'Chemistry' },
  ]
  routeSub: any;
  constructor(private _userService: UsersService,
    private route: ActivatedRoute,
    private _router: Router
  ) {
    this.studentCreateForm = new FormGroup({
      _id: new FormControl(0),
      studentCode: new FormControl(null, [Validators.required]),
      department: new FormControl(null, [Validators.required])
    })
  }
  ngOnInit(): void {
    this.routeSub = this.route.params.subscribe((params) => {
      this.Load(params['id']);
    });
  }
  Save() {
    if (this.studentCreateForm.valid) {
      this._userService.postCalls('save', this.studentCreateForm.value)
        .then(val => {
          if (val) {
            this._router.navigate(['/user/userList'])
        }
        })
    }
  }
  Load(id) {
    this._userService.getCalls('getByID/' + id)
      .then(val => {
        this.studentCreateForm.patchValue(val[0]);
      })
  }
}
