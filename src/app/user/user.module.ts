import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { UserComponent } from './user.component';
import { UserListComponent } from './user-list/user-list.component';
import { UserEditComponent } from './user-edit/user-edit.component';
import { SharedModule } from '../shared/shared.module';
import { UsersService } from './users.service';


@NgModule({
  declarations: [UserComponent, UserListComponent, UserEditComponent],
  imports: [
    CommonModule,
    UserRoutingModule,
    SharedModule
  ],
  providers:[UsersService]
})
export class UserModule { }
