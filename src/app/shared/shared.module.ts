import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { HttpClientModule } from '@angular/common/http';


let importsArray = [
  ReactiveFormsModule,
  RouterModule,
  FormsModule,
  NgbModule,
];
let decelerationArray = [

];
let entry = [ ];
let providers = [];
@NgModule({
  declarations: [...decelerationArray],
  imports: [CommonModule, ...importsArray],
  exports: [...importsArray, ...decelerationArray],
  entryComponents: [...entry],
})
export class SharedModule {}
