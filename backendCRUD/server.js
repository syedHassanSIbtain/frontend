const express = require("express");
const connectDB = require("./config/db");
const app = express();
connectDB();

app.use(express.json({ extended: false }));
app.use(function (req, res, next) {
  // Website you wish to allow to connect

  res.setHeader("Access-Control-Allow-Origin", "*");

  // Request methods you wish to allow

  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );

  // Request headers you wish to allow

  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, dataType, Authorization, responseType, enctype,Access-Control-Allow-Origin,rightid"
  );

  // Set to true if you need the website to include cookies in the requests sent

  // to the API (e.g. in case you use sessions)

  res.setHeader("Access-Control-Allow-Credentials", true);

  // Pass to next layer of middleware

  next();
});
app.use('/', require('./routes/student'));

app.listen(process.env.PORT || 5000);
