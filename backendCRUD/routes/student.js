const express = require("express");
const router = express.Router();
const config = require("config");

const student = require("../modals/student");

router.get("/getAll", async (req, res) => {
  const filter = {};
  const all = await student.find(filter);
  res.json(all);
});
router.get("/getByID/:Id", async (req, res) => {
  let _id = req.params.Id;
  const filter = { _id };
  const studentData = await student.find(filter);

  res.send(studentData);
});
router.post("/save", async (req, res) => {
  let data = req.body;
  if (data._id) {
    const studentData = await student.findOneAndUpdate({ _id: data._id }, data);
    res.json(studentData);
  } else {
    let department = data.department;
    let studentCode = data.studentCode;

    let temp = new student({
      department,
      studentCode,
      date: new Date(),
    });
    await temp.save();
    res.json(temp);
  }
});
router.get("/delete/:Id", async (req, res) => {
  let Id = req.params.Id;

  await student.findByIdAndDelete( Id);
  res.send(true);

});

module.exports = router;
