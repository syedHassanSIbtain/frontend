const mongoose = require("mongoose");

const studentSchema = new mongoose.Schema({
  studentCode: String,
  studentID: String,
  department: String,
  date: { type: String, default: Date.now },
});
module.exports = mongoose.model('student',studentSchema);